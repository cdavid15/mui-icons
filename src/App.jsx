import * as React from "react";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import {
  CaseIcon,
  DescIcon,
  DirectMeasuresIcon,
  InnKeeperIcon,
  IntranetIcon,
  IntranetAltIcon,
  MissingPersonsIcon,
  CustodyIcon,
  CustodyAltIcon,
  PNCIcon,
  PowerBIIcon,
  RTCCrashIcon,
  RTCCrashAltIcon,
  ScopeIcon,
  ShogunIcon,
  SIDIcon,
  StopSearchIcon,
  StopSearchAltIcon,
  UNIFIIcon,
  VPDIcon,
} from "./Icons";
import Stack from "@mui/material/Stack";

export default function App() {
  return (
    <Container>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Case Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <CaseIcon />
          <CaseIcon color="secondary" />
          <CaseIcon color="primary" />
          <CaseIcon color="secondary" />
          <CaseIcon color="success" />
          <CaseIcon color="action" />
          <CaseIcon color="disabled" />
          <CaseIcon color="primary" fontSize="small" />
          <CaseIcon color="primary" fontSize="large" />
          <CaseIcon color="primary" sx={{ fontSize: 70 }} />
          <CaseIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">
          Digital Evidence Sharing Capability Icons
        </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <DescIcon />
          <DescIcon color="secondary" />
          <DescIcon color="primary" />
          <DescIcon color="secondary" />
          <DescIcon color="success" />
          <DescIcon color="action" />
          <DescIcon color="disabled" />
          <DescIcon color="primary" fontSize="small" />
          <DescIcon color="primary" fontSize="large" />
          <DescIcon color="primary" sx={{ fontSize: 70 }} />
          <DescIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Direct Measures Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <DirectMeasuresIcon />
          <DirectMeasuresIcon color="secondary" />
          <DirectMeasuresIcon color="primary" />
          <DirectMeasuresIcon color="secondary" />
          <DirectMeasuresIcon color="success" />
          <DirectMeasuresIcon color="action" />
          <DirectMeasuresIcon color="disabled" />
          <DirectMeasuresIcon color="primary" fontSize="small" />
          <DirectMeasuresIcon color="primary" fontSize="large" />
          <DirectMeasuresIcon color="primary" sx={{ fontSize: 70 }} />
          <DirectMeasuresIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">InnKeeper Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <InnKeeperIcon />
          <InnKeeperIcon color="secondary" />
          <InnKeeperIcon color="primary" />
          <InnKeeperIcon color="secondary" />
          <InnKeeperIcon color="success" />
          <InnKeeperIcon color="action" />
          <InnKeeperIcon color="disabled" />
          <InnKeeperIcon color="primary" fontSize="small" />
          <InnKeeperIcon color="primary" fontSize="large" />
          <InnKeeperIcon color="primary" sx={{ fontSize: 70 }} />
          <InnKeeperIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Intranet Icons **</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <IntranetIcon />
          <IntranetIcon color="secondary" />
          <IntranetIcon color="primary" />
          <IntranetIcon color="secondary" />
          <IntranetIcon color="success" />
          <IntranetIcon color="action" />
          <IntranetIcon color="disabled" />
          <IntranetIcon color="primary" fontSize="small" />
          <IntranetIcon color="primary" fontSize="large" />
          <IntranetIcon color="primary" sx={{ fontSize: 70 }} />
          <IntranetIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Intranet (Alt) Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <IntranetAltIcon />
          <IntranetAltIcon color="secondary" />
          <IntranetAltIcon color="primary" />
          <IntranetAltIcon color="secondary" />
          <IntranetAltIcon color="success" />
          <IntranetAltIcon color="action" />
          <IntranetAltIcon color="disabled" />
          <IntranetAltIcon color="primary" fontSize="small" />
          <IntranetAltIcon color="primary" fontSize="large" />
          <IntranetAltIcon color="primary" sx={{ fontSize: 70 }} />
          <IntranetAltIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Missing Persons Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <MissingPersonsIcon />
          <MissingPersonsIcon color="secondary" />
          <MissingPersonsIcon color="primary" />
          <MissingPersonsIcon color="secondary" />
          <MissingPersonsIcon color="success" />
          <MissingPersonsIcon color="action" />
          <MissingPersonsIcon color="disabled" />
          <MissingPersonsIcon color="primary" fontSize="small" />
          <MissingPersonsIcon color="primary" fontSize="large" />
          <MissingPersonsIcon color="primary" sx={{ fontSize: 70 }} />
          <MissingPersonsIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Custody Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <CustodyIcon />
          <CustodyIcon color="secondary" />
          <CustodyIcon color="primary" />
          <CustodyIcon color="secondary" />
          <CustodyIcon color="success" />
          <CustodyIcon color="action" />
          <CustodyIcon color="disabled" />
          <CustodyIcon color="primary" fontSize="small" />
          <CustodyIcon color="primary" fontSize="large" />
          <CustodyIcon color="primary" sx={{ fontSize: 70 }} />
          <CustodyIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Custody (Alt) Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <CustodyAltIcon />
          <CustodyAltIcon color="secondary" />
          <CustodyAltIcon color="primary" />
          <CustodyAltIcon color="secondary" />
          <CustodyAltIcon color="success" />
          <CustodyAltIcon color="action" />
          <CustodyAltIcon color="disabled" />
          <CustodyAltIcon color="primary" fontSize="small" />
          <CustodyAltIcon color="primary" fontSize="large" />
          <CustodyAltIcon color="primary" sx={{ fontSize: 70 }} />
          <CustodyAltIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">PNC Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <PNCIcon />
          <PNCIcon color="secondary" />
          <PNCIcon color="primary" />
          <PNCIcon color="secondary" />
          <PNCIcon color="success" />
          <PNCIcon color="action" />
          <PNCIcon color="disabled" />
          <PNCIcon color="primary" fontSize="small" />
          <PNCIcon color="primary" fontSize="large" />
          <PNCIcon color="primary" sx={{ fontSize: 70 }} />
          <PNCIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Power BI Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <PowerBIIcon />
          <PowerBIIcon color="secondary" />
          <PowerBIIcon color="primary" />
          <PowerBIIcon color="secondary" />
          <PowerBIIcon color="success" />
          <PowerBIIcon color="action" />
          <PowerBIIcon color="disabled" />
          <PowerBIIcon color="primary" fontSize="small" />
          <PowerBIIcon color="primary" fontSize="large" />
          <PowerBIIcon color="primary" sx={{ fontSize: 70 }} />
          <PowerBIIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">RTC Crash Icons **</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <RTCCrashIcon />
          <RTCCrashIcon color="secondary" />
          <RTCCrashIcon color="primary" />
          <RTCCrashIcon color="secondary" />
          <RTCCrashIcon color="success" />
          <RTCCrashIcon color="action" />
          <RTCCrashIcon color="disabled" />
          <RTCCrashIcon color="primary" fontSize="small" />
          <RTCCrashIcon color="primary" fontSize="large" />
          <RTCCrashIcon color="primary" sx={{ fontSize: 70 }} />
          <RTCCrashIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">RTC Crash (Alt) Icons</Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <RTCCrashAltIcon />
          <RTCCrashAltIcon color="secondary" />
          <RTCCrashAltIcon color="primary" />
          <RTCCrashAltIcon color="secondary" />
          <RTCCrashAltIcon color="success" />
          <RTCCrashAltIcon color="action" />
          <RTCCrashAltIcon color="disabled" />
          <RTCCrashAltIcon color="primary" fontSize="small" />
          <RTCCrashAltIcon color="primary" fontSize="large" />
          <RTCCrashAltIcon color="primary" sx={{ fontSize: 70 }} />
          <RTCCrashAltIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">SCOPE Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <ScopeIcon />
          <ScopeIcon color="secondary" />
          <ScopeIcon color="primary" />
          <ScopeIcon color="secondary" />
          <ScopeIcon color="success" />
          <ScopeIcon color="action" />
          <ScopeIcon color="disabled" />
          <ScopeIcon color="primary" fontSize="small" />
          <ScopeIcon color="primary" fontSize="large" />
          <ScopeIcon color="primary" sx={{ fontSize: 70 }} />
          <ScopeIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">SHOGUN Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <ShogunIcon />
          <ShogunIcon color="secondary" />
          <ShogunIcon color="primary" />
          <ShogunIcon color="secondary" />
          <ShogunIcon color="success" />
          <ShogunIcon color="action" />
          <ShogunIcon color="disabled" />
          <ShogunIcon color="primary" fontSize="small" />
          <ShogunIcon color="primary" fontSize="large" />
          <ShogunIcon color="primary" sx={{ fontSize: 70 }} />
          <ShogunIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">SID Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <SIDIcon />
          <SIDIcon color="secondary" />
          <SIDIcon color="primary" />
          <SIDIcon color="secondary" />
          <SIDIcon color="success" />
          <SIDIcon color="action" />
          <SIDIcon color="disabled" />
          <SIDIcon color="primary" fontSize="small" />
          <SIDIcon color="primary" fontSize="large" />
          <SIDIcon color="primary" sx={{ fontSize: 70 }} />
          <SIDIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>

      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Stop &amp; Search Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <StopSearchIcon />
          <StopSearchIcon color="secondary" />
          <StopSearchIcon color="primary" />
          <StopSearchIcon color="secondary" />
          <StopSearchIcon color="success" />
          <StopSearchIcon color="action" />
          <StopSearchIcon color="disabled" />
          <StopSearchIcon color="primary" fontSize="small" />
          <StopSearchIcon color="primary" fontSize="large" />
          <StopSearchIcon color="primary" sx={{ fontSize: 70 }} />
          <StopSearchIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">Stop &amp; Search (Alt) Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <StopSearchAltIcon />
          <StopSearchAltIcon color="secondary" />
          <StopSearchAltIcon color="primary" />
          <StopSearchAltIcon color="secondary" />
          <StopSearchAltIcon color="success" />
          <StopSearchAltIcon color="action" />
          <StopSearchAltIcon color="disabled" />
          <StopSearchAltIcon color="primary" fontSize="small" />
          <StopSearchAltIcon color="primary" fontSize="large" />
          <StopSearchAltIcon color="primary" sx={{ fontSize: 70 }} />
          <StopSearchAltIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">UNIFI Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <UNIFIIcon />
          <UNIFIIcon color="secondary" />
          <UNIFIIcon color="primary" />
          <UNIFIIcon color="secondary" />
          <UNIFIIcon color="success" />
          <UNIFIIcon color="action" />
          <UNIFIIcon color="disabled" />
          <UNIFIIcon color="primary" fontSize="small" />
          <UNIFIIcon color="primary" fontSize="large" />
          <UNIFIIcon color="primary" sx={{ fontSize: 70 }} />
          <UNIFIIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
      <Box sx={{ my: 2 }}>
        <Typography variant="h6">VPD Icons </Typography>
        <Stack direction="row" spacing={3} alignItems={"center"}>
          <VPDIcon />
          <VPDIcon color="secondary" />
          <VPDIcon color="primary" />
          <VPDIcon color="secondary" />
          <VPDIcon color="success" />
          <VPDIcon color="action" />
          <VPDIcon color="disabled" />
          <VPDIcon color="primary" fontSize="small" />
          <VPDIcon color="primary" fontSize="large" />
          <VPDIcon color="primary" sx={{ fontSize: 70 }} />
          <VPDIcon color="primary" sx={{ fontSize: 100 }} />
        </Stack>
      </Box>
    </Container>
  );
}
